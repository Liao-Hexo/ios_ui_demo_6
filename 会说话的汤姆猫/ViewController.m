//
//  ViewController.m
//  会说话的汤姆猫
//
//  Created by 廖家龙 on 2020/4/21.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageViewCat;

- (IBAction)angrytail;//1.踩尾巴
- (IBAction)blink;//2.眨眼睛
- (IBAction)cymbal;//3.击打乐器
- (IBAction)drinkmilk;//4.喝牛奶
- (IBAction)eat;//5.吃
- (IBAction)fart;//6.放屁
- (IBAction)footleft;//7.左脚
- (IBAction)footright;//8.右脚
- (IBAction)happy;//9.很高兴
- (IBAction)happysimple;//10.一点高兴
- (IBAction)knock;//11.敲头
- (IBAction)listen;//12.听
- (IBAction)pie;//13.扔馅饼
- (IBAction)scratch;//14.刮屏幕
- (IBAction)sneeze;//15.打喷嚏
- (IBAction)stomach;//16.胃疼
- (IBAction)talk;//17.说话
- (IBAction)yawn;//18.打哈欠

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//1.踩尾巴的动画
- (IBAction)angrytail {
    [self startAnimating:26 picName:@"cat_angry"];
}

//2.眨眼睛的动画
- (IBAction)blink {
    [self startAnimating:3 picName:@"cat_blink"];
}

//3.击打乐器的动画
- (IBAction)cymbal {
    [self startAnimating:13 picName:@"cat_cymbal"];
}

//4.喝牛奶的动画
- (IBAction)drinkmilk {
    [self startAnimating:81 picName:@"cat_drink"];
}

//5.吃的动画
- (IBAction)eat {
    [self startAnimating:40 picName:@"cat_eat"];
}

//6.放屁的动画
- (IBAction)fart {
    [self startAnimating:28 picName:@"cat_fart"];
}

//7.左脚的动画
- (IBAction)footleft {
    [self startAnimating:30 picName:@"cat_foot_left"];
}

//8.右脚的动画
- (IBAction)footright {
    [self startAnimating:30 picName:@"cat_foot_right"];
}

//9.很高兴的动画
- (IBAction)happy {
    [self startAnimating:29 picName:@"cat_happy"];
}

//10.一点高兴的动画
- (IBAction)happysimple {
    [self startAnimating:25 picName:@"cat_happy_simple"];
}

//11.敲头的动画
- (IBAction)knock {
    [self startAnimating:81 picName:@"cat_knockout"];
}

//12.听的动画
- (IBAction)listen {
    [self startAnimating:12 picName:@"cat_listen"];
}

//13.扔馅饼的动画
- (IBAction)pie {
    [self startAnimating:23 picName:@"pie"];
}

//14.刮屏幕的动画
- (IBAction)scratch {
    [self startAnimating:56 picName:@"cat_scratch"];
}

//15.打喷嚏的动画
- (IBAction)sneeze {
    [self startAnimating:14 picName:@"cat_sneeze"];
}

//16.胃疼的动画
- (IBAction)stomach {
    [self startAnimating:34 picName:@"cat_stomach"];
}

//17.说话的动画
- (IBAction)talk {
    [self startAnimating:16 picName:@"cat_talk"];
}

//18.打哈欠的动画
- (IBAction)yawn {
    [self startAnimating:31 picName:@"cat_zeh"];
}

//执行动画的方法
- (void)startAnimating:(int)count picName:(NSString *)picName{
    
    //如果当前图片框正在执行动画，那么直接return，什么都不做(不开启一个新动画)
    if(self.imageViewCat.isAnimating){
        return;
    }
    
    //1.动态加载图片到一个NSArray中
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<count;i++){
        
        //拼接图片名称
        NSString *imgName=[NSString stringWithFormat:@"%@%0004d.jpg",picName,i];
        
        //第一种加载图片的方式：
        //根据imageNamed图片名称加载图片，加载好的图片会一直保存写在内存中，不会释放，这样下次如果再使用同样的图片的时候就不需要再重新加载了，因为内存里面已经有了；缺点就是如果加载了大量的图片，那么这些图片会一直保留在内存中，导致应用程序占用内存过大(这就叫缓存)
        // UIImage *imgCat=[UIImage imageNamed:imgName];
        
        //第二种加载图片的方式
        //1.获取图片的完整路径
        NSString *path=[[NSBundle mainBundle] pathForResource:imgName ofType:nil];
        //2.这里的参数不能再传递图片名称了，这里需要传递一个图片的完整路径
        UIImage *imgCat=[UIImage imageWithContentsOfFile:path];
        
        //把图片加载到数组中
        [arrayM addObject:imgCat];
    }
    
    //2.设置UIImageView（图片框）的animationImages属性，这个属性中包含的就是所有那些要执行动画的图片
    self.imageViewCat.animationImages=arrayM;
    
    //3.设置动画持续时间
    self.imageViewCat.animationDuration=self.imageViewCat.animationImages.count*0.1;
    
    //4.设置动画重复的次数
    self.imageViewCat.animationRepeatCount=1;
    
    //5.开启动画
    [self.imageViewCat startAnimating];
    
    //清空图片集合（用第二种加载图片的方式执行完动画后内存被释放）:
    //这样写的问题是，当动画启动以后，动画还没开始执行，就已经让图片集合清空了
    //self.imageViewCat.animationImages=nil;
    
    //方法：设置图片框在调用setAnimationImages:nil方法的时候延迟执行
    [self.imageViewCat performSelector:@selector(setAnimationImages:) withObject:nil afterDelay:self.imageViewCat.animationImages.count*0.1];
    
}

@end
