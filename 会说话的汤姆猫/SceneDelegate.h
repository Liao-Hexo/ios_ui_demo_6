//
//  SceneDelegate.h
//  会说话的汤姆猫
//
//  Created by 廖家龙 on 2020/4/21.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

